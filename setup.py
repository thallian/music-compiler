#!/usr/bin/env python

from distutils.core import setup

setup(name='music-compiler',
    version='0.0.1',
    description='Encode my flac collection to opus.',
    author='Sebastian Hugentobler',
    author_email='sebastian@vanwa.ch',
    url='https://gitlab.com/thallian/music-compiler',
    license='MPL 2.0',
    scripts=['music-compiler'],
    install_requires=[
        'configparser',
    ],
)
